## Übung(en): Terraform

Dazu Erstellen wir zwei VMs:
* eine VM mit dem Apache Web Server, PHP und Adminer
* eine VM mit der MySQL Datenbank

Die Übungen finden in der [Git/Bash](https://git-scm.com/downloads) statt. 

Ausserdem müssen [Multipass](https://multipass.run/) und [Terraform](https://www.terraform.io/) installiert sein.

Clont dieses Repository und wechselt in das erstellte Verzeichnis

    git clone https://gitlab.com/ch-mc-b/cdi/infra/02-terraform.git
    cd 02-terraform
    
Für die weiteren Übungen brauchen wir eine Entwicklungs, Build und Produktionsumgebung.

In einem ersten Schritt werden wir die Entwicklungs und Build (CD/CD) Umgebung anlegen. Dazu verwenden wir Terraform.

Terraform Initialisieren und Umgebung anlegen:

    terraform init
    terraform apply
    
Nach der Installation wird eine Introseite angezeigt mit weiteren Informationen.       
